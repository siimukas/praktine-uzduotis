var elements = document.getElementsByClassName("hoverable");
for (var i in elements) {
    if (!elements.hasOwnProperty(i)) continue;
    elements[i].addEventListener('mouseover', function () {
        this.querySelector("span").classList.add("hovered");
    },
        elements[i].addEventListener("mouseout", function () {
            this.querySelector("span").classList.remove("hovered");
        }))
}