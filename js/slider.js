window.onload = function(){
    var slides = document.getElementsByClassName("slide");
    var slidesCount = slides.length;
    var i = 0;

    document.querySelector(".nextSlide span").addEventListener("click", function(){
        slides[i].classList.remove("showSlide"); 
        i++;

        if(i >= slidesCount){
            i = 0;
        }
        slides[i].classList.add("showSlide");
    });

    document.querySelector(".previousSlide span").addEventListener("click",function(){

        if(i == 0){
            slides[i].classList.remove("showSlide");
            i = slidesCount-1;
            slides[i].classList.add("showSlide");
        }else{
            slides[i].classList.remove("showSlide");
            i--;
            slides[i].classList.add("showSlide");
        }
        
    });
}